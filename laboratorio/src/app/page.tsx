import styles from "./page.module.css";
import Icon from "@/component/Icon";
import Title from "@/component/Title";
import Ubicacion from "@/component/Ubicacion";
import Badge from "@/component/Badge";

export default function Home() {
  return (
    <main className={styles.main}>
      <section className="container sub-hero">
          <div className="bg2-hero"></div>
            <div className="icons">
              <div className="icons-companys">
                <Icon imagen="/imagenes/icon1.svg"></Icon>
                <Icon imagen="/imagenes/icon2.svg"></Icon>
                <Icon imagen="/imagenes/icon3.svg"></Icon>
                <Icon imagen="/imagenes/icon4.svg"></Icon>
                <Icon imagen="/imagenes/icon5.svg"></Icon>
              </div>
            </div>
          <div className="bg3-hero"></div>
      </section>
      <section className="container referencias">
        <div className="container">
          <div>
          <Title title="We offer best services" subtible="key features" />
          <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
          </div>
          <div>
            <Ubicacion imagen="/imagenes/ubicacion.svg" title="We offer best services" subtitle="Lorem Ipsum is not simply random text"></Ubicacion>
            <Ubicacion imagen="/imagenes/calendario.svg" title="Schedule your trip" subtitle="It has roots in a piece of classical"></Ubicacion>
            <Ubicacion imagen="/imagenes/compouns.svg" title="Get discounted coupons" subtitle="Lorem Ipsum is not simply random text"></Ubicacion>
          </div> 
        </div>
        <div>
          <Badge icon="icon-one" text="Paradise on Earth"></Badge>
          <img src="/imagenes/path.svg" alt="" />
          <div className="images-location">
            <img src="/imagenes/rectangle1.svg" alt="" />
            <img src="/imagenes/rectangle2.svg" alt="" />
          </div>
        </div>
      </section>
    </main>
  );
}
