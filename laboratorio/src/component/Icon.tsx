interface IconProps{
    imagen:any
}
export default function Icon({imagen}:IconProps){
    return(
            <i><img src={imagen} alt="" /></i>
    )
}