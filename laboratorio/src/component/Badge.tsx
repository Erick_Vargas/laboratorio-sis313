type BadgeProps = {
    text:string,
    icon:string,
    reverse?:boolean,
}

const Badge = (props:BadgeProps) =>{
    const {text, icon, reverse} = props;
    const defaultClass = `icon ${icon}`
    return(
        <div className={reverse?'badge-reverse badge':'badge'}>
            <i className={defaultClass}></i>
            <span>{text}</span>
        </div>
    )
}

export default Badge;
