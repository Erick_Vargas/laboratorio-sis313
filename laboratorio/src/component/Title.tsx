interface TitleProps{
    title:string,
    subtible:string,
    sizeTextTitle?:number
}

const Title=({title,subtible,sizeTextTitle}:TitleProps)=>{
    return(
        <div className="title">
            <h4 style={{textTransform:"uppercase"}}>{subtible}</h4>
            <h3 style={{fontSize:sizeTextTitle?`${sizeTextTitle}px`:"30px"}}>{title}</h3>
        </div>
    )
}

export default Title