interface UbicacionProps{
    imagen:any,
    title:string,
    subtitle:string
}

const Ubicacion =({imagen,title,subtitle}:UbicacionProps)=>{
    return(
        <div className="location">
            <img src={imagen} alt="" />
            <div>
                <h2>{title}</h2>
                <h4>{subtitle}</h4>
            </div>
        </div>
    )
}
export default Ubicacion